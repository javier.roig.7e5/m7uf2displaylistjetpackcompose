package com.example.affirmations

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.affirmations.data.Datasource
import com.example.affirmations.model.Affirmation
import com.example.affirmations.ui.theme.AffirmationsTheme
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ExpandMore
import androidx.annotation.StringRes
import androidx.compose.material.icons.filled.ExpandLess
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.runtime.*

class MainActivity : ComponentActivity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContent {
      AffirmationApp()
    }
  }
}

@Composable
fun AffirmationApp() {
  AffirmationsTheme {
    AffirmationList(affirmationList = Datasource().loadAffirmations())
  }
}

@Composable
fun AffirmationList(affirmationList: List<Affirmation>, modifier: Modifier = Modifier) {
  LazyColumn {
    items(affirmationList) { affirmation ->
      AffirmationCard(affirmation)
    }
  }
}

@Composable
fun AffirmationCard(affirmation: Affirmation, modifier: Modifier = Modifier) {
  var expanded by remember { mutableStateOf(false) }
  Card(modifier = Modifier.padding(8.dp), elevation = 4.dp) {
    Column(modifier = Modifier
      .animateContentSize(animationSpec = spring(
        dampingRatio = Spring.DampingRatioMediumBouncy,
        stiffness = Spring.StiffnessLow
      ))) {
      Image(
        painter = painterResource(affirmation.imageResourceId),
        contentDescription = stringResource(affirmation.stringResourceId),
        modifier = Modifier
          .fillMaxWidth()
          .height(194.dp),
        contentScale = ContentScale.Crop
      )
      Text(
        text = LocalContext.current.getString(affirmation.stringResourceId),
        modifier = Modifier.padding(16.dp),
        style = MaterialTheme.typography.h6
      )

      Row(
        modifier = Modifier
          .fillMaxWidth()
          .padding(8.dp)
      ) {
        /*CardIcon(affirmation.imageResourceId)
        CardInformation(affirmation.stringResourceTitle, affirmation.stringResourceDescription)*/
        Spacer(Modifier.weight(1f))
        CardItemButton(
          expanded = expanded,
          onClick = { expanded = !expanded }
        )
      }
      if (expanded) {
        CardDetails(affirmation.stringResourceDescription)
      }
    }
  }
}

@Composable
private fun CardItemButton(
  expanded: Boolean,
  onClick: () -> Unit,
  modifier: Modifier = Modifier
) {
  IconButton(onClick = onClick) {
    Icon(
      imageVector = if (expanded) Icons.Filled.ExpandLess else Icons.Filled.ExpandMore,
      tint = MaterialTheme.colors.secondary,
      contentDescription = stringResource(R.string.expand_button_content_description)
    )
  }

}

@Preview
@Composable
private fun AffirmationCardPreview() {
  //AffirmationCard (Affirmation(R.string.affirmation1, R.drawable.image1))
}

@Composable
fun CardDetails(@StringRes cardDetail: Int, modifier: Modifier = Modifier) {
  Column(
    modifier = modifier.padding(
      start = 16.dp,
      top = 8.dp,
      bottom = 16.dp,
      end = 16.dp
    )
  ) {
    Text(
      text = stringResource(R.string.title1),
      style = MaterialTheme.typography.h3,
    )
    Text(
      text = stringResource(cardDetail),
      style = MaterialTheme.typography.body1,
    )
  }
}